# alegra-bauhaus

Proje Grunt task runner kullanılarak AngularJS temelli olarak hazırlanmıştır. Deneme uygulaması olduğundan bir çok fonksiyon çalışmamaktadır

## Çalıtırma

Öncelikle bilgisayarınızda yüklü değilse Node.JS > 4.x.x olan bir sürüm kurun
Daha sonra komut satırından 'npm install -g bower grunt-cli yo' komutunu çalıştırın
Bunlar kurulduktan sonra bilgisayarınızda Chrome yüklü olduğu taktirde projeyi çalıtırmaya başlayabilirsiniz.
Proje ilk çalıştırmadan önce sırası ile dizin içerisinde "npm install" ve "bower install" komutlarını çalıştırın. 
Gerekli paketler kurulduktan sonra grunt serve komutu ile projeyi development için çalıtırabilirsiniz. Kodda yapılan değişiklikler otomatik olrak yenilenecektir.
