'use strict';

/**
 * @ngdoc function
 * @name alegraBauhausApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the alegraBauhausApp
 */
angular.module('alegraBauhausApp')
  .controller('MainCtrl', function ($scope, $http, $rootScope) {


    /**
     *@ngdoc  // function
     *@description  // HTTP çağrısı (promise). Lokal json datası kullanıldı sunucu yanıt vermiyor:
     *
     */
    //
    $http({
      method: 'GET',
/*
      url: 'http://bauhaus.multiscreen.technology/ca-mobile-product-detail?product_id=28234&response_type=json&app_version=3.0'
*/
       url: 'datas/23.json'

    }).then(function successCallback(response) {
      // Bu çağrı asenkron olarak gerçekleştirilecektir. Cevap geldiğinde otomatik olarak dönecektir.
      $rootScope.gelenData = response.data;
      setMainFields($rootScope.gelenData);
      $rootScope.urunIsmi = response.data.header.data.title_c.text;
    }, function errorCallback(response) {
      // Herhangi bir hata oluşması durumunda asenkron çağrı error dönecektir. [TODO]: Bu kısım handle edilmelidir.
    });

    /**
     *@ngdoc  // function
     *@name .: // setMainFields
     *@param {||}   // response.data
     *@param {type1=}  -  //object / json
     *@description  //JSON'dan gelen kompleks dataları rootScope ekseninde değişkenlere atıyarak daha kolay işlem yapılması sağlanıyor.
     *
     */
    var setMainFields = function (a) {
      $rootScope.page_info = a.page_info;
      $rootScope.page_type = a.page_type;
      $rootScope.background_color = a.background_color;
      $rootScope.hit_image_url = a.hit_image_url;
      $rootScope.header = a.header;
      $rootScope.navigation = a.navigation;
      $rootScope.popup = a.popup;
      $rootScope.session = a.session;
      $rootScope.pull_to_refresh = a.pull_to_refresh;
      $rootScope.shake_link = a.shake_link;
      $rootScope.status_bar_color = a.status_bar_color;
      $rootScope.progress_colour = a.progress_colour;
      $rootScope.hide_menu_shadow = a.hide_menu_shadow;
      $rootScope.language = a.language;
      $rootScope.progress_text = a.progress_text;
      $rootScope.google_tracking_id = a.google_tracking_id;
      $rootScope.page_url = a.page_url;
      $rootScope.page_title = a.page_title;
      $rootScope.page_description = a.page_description;
      $rootScope.widgets = a.widgets;

      // Widgetların componentlerinin sıraları vs önemli olmadığından mevcut componentleri bulup ilgili değişkenlere atayacak fonksiyon çağrılıyor
      setWidgetComponents($rootScope.widgets.left_content.renders);


    };
    /**
     *@ngdoc  // function
     *@name .: // setWidgetComponents
     *@param     a
     *@param     object / json
     *@description  // Sunucudan gelen dizinin widgets kısmını buraya gönderiyoruz ve ilgili componentler yakalanıyor
     *
     */
    var setWidgetComponents = function (a) {
      for (var i = 0; i < a.length; i++) {
        var currentText = a[i].module_name;
        switch (currentText) {
          case "ca_mobile_product_detail_init":
            $rootScope.prDetailInit = a[i];
            break;
          case "ca_mobile_product_detail_photos":
            $rootScope.prDetailPhotos = a[i];
            for (var s = 0; s < a[i].components.length; s++) {
              var compareText = a[i].components[s].component_name;
              switch (compareText) {
                case "ProductDetailStyle2":
                  $rootScope.prDetailStyle2 = a[i].components[s];
                  break;
                default:
                  console.log("Detail Photos İçin Component Yok");
              }
            }
            break;
          case "ca_mobile_product_detail_count_add_basket":
            $rootScope.prDetailCountAddBasket = a[i];
            for (var s = 0; s < a[i].components.length; s++) {
              var compareText = a[i].components[s].component_name;
              switch (compareText) {
                case "DoubleButton":
                  $rootScope.prButtonArray = a[i].components[s].data.components;
                  break;
                default:
                  console.log("Detail Photos İçin Component Yok");
              }
            }
            break;
          case "ca_mobile_product_detail_options":
            $rootScope.prDetailOptions = a[i];
            break;
          case "ca_mobile_product_detail_shop_list_button":
            $rootScope.prDetailShopListButton = a[i];
            for (var s = 0; s < a[i].components.length; s++) {
              var compareText = a[i].components[s].component_name;
              switch (compareText) {
                case "Button":
                  $rootScope.prShopArray = a[i].components[s];
                  break;
                default:
                  console.log("Detail Photos İçin Component Yok");
              }
            }
            break;
          case "ca_mobile_product_detail_description":
            $rootScope.prDetailDescription = a[i];
            for (var s = 0; s < a[i].components.length; s++) {
              var compareText = a[i].components[s].component_name;
              switch (compareText) {
                case "DoubleText":
                  $rootScope.prRightDescText = a[i].components[s];
                  break;
                case "ExpandableText":
                  $rootScope.prExpandableData = a[i].components[s];
                default:
                  console.log("Detail Photos İçin Component Yok");
              }
            }
            break;
          case "ca_mobile_section":
            $rootScope.mobileSection = a[i];
            break;
          case "ca_mobile_double_slide":
            $rootScope.mobileDoubleSlide = a[i];
            break;
          default:
            console.log("modül bekleniyor");
        }
      }
    };


  });
