'use strict';

/**
 * @ngdoc overview
 * @name alegraBauhausApp
 * @description
 * # alegraBauhausApp
 *
 * Uygulamanın ana modulu routing vs burada yapılıyor
 */
angular
  .module('alegraBauhausApp', [
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
